<?php
	session_start();
?>
<!DOCTYPE html>
<html>

<head>
<title>Day05</title>

<style>
input {
  padding: 15px 15px;
  margin: 8px 0;
  box-sizing: border-box;
}
div.b {
  background-color: white;
  padding: 0px;
  border: 2px solid indigo;
  margin: 15px;
}
div.c{
	text-align:center;
}
span.d {
  display:inline-block;
  background-color:limegreen;
  color:white;
  height: 20px;
  width:100px;
  padding: 15px;
  border: 1px solid indigo;
  margin: 15px;
}
.required:after {
	content:" *";
    color: red;
}
span.error{
	color:red;
	padding:20px;
	margin:20px;
}

.button {
  background-color: limegreen;
  border: 1px solid indigo;
  color: white;
  margin:15px;
  width:100px;
  padding: 15px;
  text-align: center;
  display: inline-block;
  font-size: 16px;
}
</style>
</head>

<body>
<?php
date_default_timezone_set ("Asia/Ho_Chi_Minh");
$nameErr = $genderErr = $facultyErr = $birthdayErr = $imgErr = "";
  
if ($_SERVER["REQUEST_METHOD"] == "POST") {  
      
    if (empty($_POST["name"])) {  
		$nameErr = "Hãy nhập tên";  
    }
	else {
		$_SESSION["name"] = $_POST["name"];
	}
	if (empty ($_POST["gender"])) {  
		$genderErr = "Hãy chọn giới tính";  
    }
	else {
		$_SESSION["gender"] = $_POST["gender"];
	}
	if (empty ($_POST["khoa"])) {  
		$facultyErr = "Hãy chọn phân khoa";  
    }
	else {
		$_SESSION["khoa"] = $_POST["khoa"];
	}
	if (empty ($_POST["birthday"])) {  
		$birthdayErr = "Hãy nhập ngày sinh";   
    }
	else {
		$_SESSION["birthday"] = $_POST["birthday"];
	}
	$_SESSION["address"] = $_POST["address"];
	
	if (!file_exists('upload/')) {
		mkdir('upload/', 0777, true);
	}
	$file = $_FILES["picture"];
	$filename    = $file["tmp_name"];
	$finfo 		= finfo_open(FILEINFO_MIME_TYPE);
	$file_type  = finfo_file($finfo, $filename);
	$accept = array("image/jpg", "image/png", "image/jpeg", "image/gif");
	if(!in_array((string)$file_type, $accept)){
		$imgErr = "Không phải hình ảnh. Hãy nhập hình ảnh";
	}
	else {
		$filerealname = $file["name"];
		$location = "upload/";
		$str_name = explode('.',$filerealname);
		$extension = end($str_name);
		$newname = reset($str_name)."_".date("YmdHis").".".$extension;
		$destination = $location.$newname; 
		move_uploaded_file($filename, $destination);
		$_SESSION["myfile"] = $destination;
	}
	if ($nameErr == "" && $genderErr == "" && $facultyErr == "" && $birthdayErr == "" && $imgErr == ""){
		header("Location: confirm.php");
		exit();
	}
}	
?>
	<div class = "b">
		<span class="error">
			<?php
				echo $nameErr;
			?>
		</span></br>
		<span class="error">
			<?php
				echo $genderErr;
			?>
		</span></br>
		<span class="error">
			<?php
				echo $facultyErr;
			?>
		</span></br>
		<span class="error">
			<?php
				echo $birthdayErr;
			?>
		</span></br>
		<span class="error">
			<?php
				echo $imgErr;
			?>
		</span></br>
		<form method = "post" enctype="multipart/form-data">
			<div class = "container">
				<span class = "d"> <div class = "required"> Họ và tên </div></span>
				<input type="text" id="name" name="name">
			</div>
			<div class = "container">
				<span class = "d"> <div class = "required"> Giới tính </div></span>
				<?php
				$arr1 = array("Nam", "Nữ");
				for ($x = 0; $x < 2; $x++){ ?>
					<input type="radio" id=" <?php echo $x; ?>" name="gender" value="<?php echo $arr1[$x]; ?>">
					<label for="<?php echo $x; ?>"> <?php echo $arr1[$x]; ?> </label>
				<?php } ?>
			</div>
			
			<div class = "container">
				<span class = "d"> <div class = "required"> Phân khoa </div></span>
				<select name="khoa" id="khoa">
				<?php
					$arr = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
					foreach($arr as $key=>$value){ ?>
					<option value = "<?php echo $key; ?>" > <?php echo $value; ?> </option>
				<?php } ?>
				</select>
			</div>
			<div class = "container">
				<span class = "d"> <div class = "required"> Ngày sinh </div></span>
				<input type="date" id="birthday" name="birthday"/>
			</div>
			
			<div class = "container">
				<span class = "d"> Địa chỉ </span>
				<input type="text" id="address" name="address">
			</div>
			<div class = "container">
				<span class = "d">  <label for="picture">Hình ảnh</label> </span>
				<input type = "file" id = "picture" name = "picture" accept="image/*">
			</div>
			<div class = "c">
				<button class = "button">Đăng ký</button>
			</div>
			</form>
		</div>
	</body>
</html>